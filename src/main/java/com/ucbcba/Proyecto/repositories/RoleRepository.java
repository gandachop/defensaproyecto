package com.ucbcba.Proyecto.repositories;

import com.ucbcba.Proyecto.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
    public interface RoleRepository extends JpaRepository<Role, Integer> {

    }
