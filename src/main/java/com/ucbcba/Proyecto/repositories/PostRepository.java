package com.ucbcba.Proyecto.repositories;

import com.ucbcba.Proyecto.entities.Post;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface PostRepository extends CrudRepository<Post, Integer> {

}
