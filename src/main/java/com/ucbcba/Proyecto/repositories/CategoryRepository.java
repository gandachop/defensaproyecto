package com.ucbcba.Proyecto.repositories;

import com.ucbcba.Proyecto.entities.Category;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface CategoryRepository extends CrudRepository<Category, Integer> {

}
