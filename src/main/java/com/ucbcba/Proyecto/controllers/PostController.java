package com.ucbcba.Proyecto.controllers;

import com.ucbcba.Proyecto.entities.Post;
import com.ucbcba.Proyecto.services.CategoryService;
import com.ucbcba.Proyecto.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
@Controller
public class PostController {

    PostService postService;
    CategoryService categoryService;

    @Autowired
    public void setPostService(PostService postService){
        this.postService = postService;
    }

    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public String index( Model model) {
        List<Post> posts  = (List) postService.listAllPost();
        model.addAttribute("posts", posts);
        return "posts";
    }

    @RequestMapping(value = "/post/edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable Integer id, Model model) {
        Post post = postService.findPost(id);
        model.addAttribute("post", post);
        return "editPost";
    }

    @RequestMapping(value = "/post/{id}", method = RequestMethod.GET)
    public String show(@PathVariable Integer id, Model model) {
        Post post = postService.findPost(id);
        model.addAttribute("post", post);
        return "showPost";
    }

    @RequestMapping(value = "/post/new", method = RequestMethod.GET)
    public String newPost( Model model) {

        model.addAttribute("post", new Post());
        model.addAttribute("categories", (List)categoryService.listAllCategory());
        return "newPost";
    }

    @RequestMapping(value = "/post", method = RequestMethod.POST)
    public String create(@ModelAttribute("post") Post post, Model model) {

        postService.savePost(post);
        return "redirect:/welcome";
    }

    @RequestMapping(value = "/post/delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable Integer id, Model model) {
        postService.deletePost(id);
        return "redirect:/welcome";
    }


}
