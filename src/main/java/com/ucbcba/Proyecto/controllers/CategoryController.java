package com.ucbcba.Proyecto.controllers;

import com.ucbcba.Proyecto.entities.Category;
import com.ucbcba.Proyecto.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class CategoryController {
    CategoryService categoryService;
    //CategoryRepository categoryRepository;

    @Autowired
    public void setCategoryService(CategoryService categoryService){
        this.categoryService=categoryService;
    }

    @RequestMapping(value = "/categories", method = RequestMethod.GET)
    public String index( Model model) {
        List<Category> categories  = (List) categoryService.listAllCategory();
        model.addAttribute("categories", categories);
        return "categories";
    }

    @RequestMapping(value = "/category/edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable Integer id, Model model) {
        Category  category = categoryService.findCategory(id);
        model.addAttribute("category", category);
        return "editCategory";
    }

    @RequestMapping(value = "/category/{id}", method = RequestMethod.GET)
    public String show(@PathVariable Integer id, Model model) {
        Category category = categoryService.findCategory(id);
        model.addAttribute("category", category);
        return "showCategory";
    }

    @RequestMapping(value = "/category/new", method = RequestMethod.GET)
    public String newCategory( Model model) {
       /* List<Category> categories  =
                (List) categoryService.listAllCategory();

        model.addAttribute("category", new Category());

        model.addAttribute("categories", categories);*/

        return "newCategory";
    }

    @RequestMapping(value = "/category", method = RequestMethod.POST)
    public String create(@ModelAttribute("category") Category category, Model model) {
        if(category.getName()!= null)
        {
            categoryService.saveCategory(category);
        }
        return "redirect:/categories";
    }

    @RequestMapping(value = "/category/delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable Integer id, Model model) {
        categoryService.deleteCategory(id);
        return "redirect:/categories";
    }


}
