package com.ucbcba.Proyecto.services;

import com.ucbcba.Proyecto.entities.Role;

public interface RolesService {
    Iterable<Role> listAllOptions();
    Role getRoleById(Integer id);
    Role saveOption(Role role);
    void deleteOption(Integer id);
}

