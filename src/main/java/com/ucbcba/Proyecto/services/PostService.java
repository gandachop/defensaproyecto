package com.ucbcba.Proyecto.services;

import com.ucbcba.Proyecto.entities.Post;

public interface PostService {

    Iterable<Post> listAllPost();
    Post findPost(Integer id);
    void savePost(Post post);
    void deletePost(Integer id);
}
