package com.ucbcba.Proyecto.services;

import com.ucbcba.Proyecto.entities.Category;

public interface CategoryService {

    Iterable<Category> listAllCategory();
    Category findCategory(Integer id);
    void saveCategory(Category category);
    void deleteCategory(Integer id);
}
