package com.ucbcba.Proyecto.services;

import com.ucbcba.Proyecto.entities.Post;
import com.ucbcba.Proyecto.repositories.PostRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PostServiceImpl implements PostService {

    PostRepository postRepository;


    @Override
    public Iterable<Post> listAllPost() {
        return null;
    }

    @Override
    public Post findPost(Integer id) {
        Optional<Post> opt;
        opt = postRepository.findById(id);
        return opt.get();
    }

    @Override
    public void savePost(Post post) {
        postRepository.save(post);
    }

    @Override
    public void deletePost(Integer id) {
        postRepository.deleteById(id);
    }
}
