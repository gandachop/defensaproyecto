package com.ucbcba.Proyecto.services;

import com.ucbcba.Proyecto.entities.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}